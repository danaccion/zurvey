<?php

namespace App\Data\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Data\Models\BaseModel;


class ReadingsModel extends BaseModel
{
    use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'readings';
    // protected $appends = [
    //    'value','name'
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'meter_id','reading1','reading2','reading3','reading4','receivedTime'
    ];

   
}

<?php

namespace App\Data\Repositories;

use App\Data\Models\ReadingsModel;
use App\Data\Repositories\BaseRepository;
use App\Readings;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ReadingsRepository extends BaseRepository
{

    protected $readings;

    public function __construct(ReadingsModel $readings) 
    {
        $this->readings = $readings;
    }

    public function fetchReadings($data = [])
    {
        $meta_index = "readings";
        $parameters = [];
        $count = 0;

        if (isset($data['id']) &&
            is_numeric($data['id'])) {

            $meta_index = "readings";
            $data['single'] = true;
            $data['where'] = [
                [
                    "target" => "id",
                    "operator" => "=",
                    "value" => $data['id'],
                ],
            ];

            $parameters['id'] = $data['id'];

        }

        $count_data = $data;

        // $data['relations'][] = 'info';

        $result = $this->fetchGeneric($data, $this->readings);

        if (!$result) {
            return $this->setResponse([
                'code' => 404,
                'title' => "No agents are found",
                "meta" => [
                    $meta_index => $result,
                ],
                "parameters" => $parameters,
            ]);
        }

        // $count = $this->countData($count_data, refresh_model($this->products->getModel()));

        return $this->setResponse([
            "code" => 200,
            "title" => "Successfully retrieved agents",
            "meta" => [
                $meta_index => $result,
                // "count" => $count,
            ],
            "parameters" => $parameters,
        ]);
    }

    public function create($data = [])
    {
        // data validation
        $result = [];
        $product_id = 0;
        foreach ($data as $key => $value) {
           
            array_push($result, $value);
        }

        for($i = 0 ; $i < count($result) ; $i++){

        
    /*        if (!isset($result[$i]['meter_id'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "meter_id is not set.",
                ]);
            }

		  if (!isset($result[$i]['receivedTime'])) {
                return $this->setResponse([
                    'code'  => 500,
                    'title' => "receivedTime is not set.",
                ]);
            }
*/
            $readings = $this->readings->init($this->readings->pullFillable($result[$i]));
            $readings->save($result[$i]);
        }
            if (!$readings->save($result)) {
                return $this->setResponse([
                    "code"        => 500,
                    "title"       => "Data Validation Error.",
                    "description" => "An error was detected on one of the inputted data.",
                    "meta"        => [
                        "errors" => $readings->errors(),
                    ],
                ]);
            }

            return $this->setResponse([
                "code"       => 200,
                "title"      => "Successfully create readings.",
                "parameters" => $result,
            ]);
        
    }

    public function update($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        if (!isset($data['reserved_id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "reserved_id is not set.",
            ]);
        }
        
        if (!isset($data['approved'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "approved is not set.",
            ]);
        }

        if (!isset($data['rejected'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "rejected is not set.",
            ]);
        }
        $readings = $this->readings->find($data['id']);
        if($readings==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "Coach not found.",
            ]);
        }

        $readings->save($data);
        if (!$readings->save($data)) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $readings->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully updated a readings.",
            "meta"        => [
                "status" => $readings,
            ]
        ]);
            
        
    }

    public function delete($data = [])
    {
        if (!isset($data['id'])) {
            return $this->setResponse([
                'code'  => 500,
                'title' => "id is not set.",
            ]);
        }

        $readings = $this->readings->find($data['id']);
        if($readings==null){
            return $this->setResponse([
                'code'  => 500,
                'title' => "product not found.",
            ]);
        }
        
        if (!$readings->delete()) {
            return $this->setResponse([
                "code"        => 500,
                "title"       => "Data Validation Error.",
                "description" => "An error was detected on one of the inputted data.",
                "meta"        => [
                    "errors" => $readings->errors(),
                ],
            ]);
        }

        return $this->setResponse([
            "code"       => 200,
            "title"      => "Successfully deleted a readings.",
            "meta"        => [
                "status" => $readings,
            ]
        ]);
            
        
    }


}

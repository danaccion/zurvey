-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2020 at 12:40 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zurvay`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2019_11_14_064227_received_json_table', 2),
(10, '2020_02_17_083246_create_table_readings', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('1f27c6f8be32cbbf8b9fb223ef7ae84cc045e22352be13a5ac1f97408fef809b9e00914c68dafb5c', 9, 5, 'free4kids', '[]', 0, '2020-04-01 03:52:26', '2020-04-01 03:52:26', '2021-04-01 11:52:26'),
('2c02a2eb9891ff53746508214fda100a9618122dc164666677a1faedec478a316cffc577e6fb2dee', 1, 3, 'free4kids', '[]', 0, '2020-02-14 03:33:17', '2020-02-14 03:33:17', '2021-02-14 11:33:17'),
('3080d0108a271c44eefff192396ccec30c38f3307bb6bd48a8ca69e654d3d170e6e1f0b1ee6971d3', 1, 3, 'free4kids', '[]', 0, '2020-02-14 03:32:40', '2020-02-14 03:32:40', '2021-02-14 11:32:40'),
('322cefd6bb564cb13ddeee857e8b2f174e579f0ef53f5d2743592d3fcd38ab416f09774d72d30416', 1, 3, 'free4kids', '[]', 0, '2020-02-29 00:53:55', '2020-02-29 00:53:55', '2021-03-01 08:53:55'),
('46a32c6878999a5e26b851d3eb80330efdd646e96144831e9eaaa3c2938e607e46d9c814db33938e', 7, 5, 'free4kids', '[]', 0, '2020-04-01 03:31:49', '2020-04-01 03:31:49', '2021-04-01 11:31:49'),
('5cc1ebe5b7efdd6fd88d496799e9954ed63e70505395fe684f85955f38068826fe22d8216d043fde', 2, 3, 'free4kids', '[]', 0, '2020-02-17 00:16:08', '2020-02-17 00:16:08', '2021-02-17 08:16:08'),
('6d23b6ae5996902f5c5049047b3a58d7e901bde541fce0f85ed0ff073a4ce9cd86bd1a67db095491', 7, 5, 'free4kids', '[]', 0, '2020-04-03 06:02:09', '2020-04-03 06:02:09', '2021-04-03 14:02:09'),
('6f1ec06c29d7df05c816da7499d144a65b013f13dc698f66e54390ed70127bdbb694e99bab52c0eb', 7, 5, 'free4kids', '[]', 0, '2020-04-01 03:27:18', '2020-04-01 03:27:18', '2021-04-01 11:27:18'),
('711af7828c8c16440794b762c498e019a4cad988da0c711fe562edbd5d98f957685049c7909becae', 1, 3, 'free4kids', '[]', 0, '2020-02-29 00:53:10', '2020-02-29 00:53:10', '2021-03-01 08:53:10'),
('7180337c2d5c40d4eda9600c55c34c6a5d4d115b1a0138012ec10b6463dea80ca03c2e2b1025f690', 1, 3, 'free4kids', '[]', 0, '2020-02-29 00:52:54', '2020-02-29 00:52:54', '2021-03-01 08:52:54'),
('80f7165e036a003dcb8ac6214735046911acc0bb462f3cbf97a56c13b37abb73c79a24d82a8999a9', 7, 5, 'free4kids', '[]', 0, '2020-03-30 02:42:52', '2020-03-30 02:42:52', '2021-03-30 10:42:52'),
('8b3ab2f84f76a82e1ca7803a09af28f02e1d63c25e07edbf73c8d1cfa4981d4c11ba7d5f2c370656', 2, 3, 'free4kids', '[]', 0, '2020-02-17 00:16:39', '2020-02-17 00:16:39', '2021-02-17 08:16:39'),
('96a0b3bd9ad9ff79f3ca88486bb39935c864a5dbc5bf8d793fe65ad8563296cd9ee42a2541d841aa', 1, 3, 'free4kids', '[]', 0, '2020-02-29 00:53:43', '2020-02-29 00:53:43', '2021-03-01 08:53:43'),
('9fa1dd6733069cfeb7cc4630fe47e4051583d52f7017928cb24860c6f299463b279ed3687fa260f0', 1, 3, 'free4kids', '[]', 0, '2020-02-29 00:52:48', '2020-02-29 00:52:48', '2021-03-01 08:52:48'),
('cad50bc17dfe6c85105296f2a5204ba839b9e1d06a2c72cec749c1367c7454d713e2f0e993433bbf', 7, 5, 'free4kids', '[]', 0, '2020-04-03 06:02:39', '2020-04-03 06:02:39', '2021-04-03 14:02:39'),
('cd878c31375a597d933cbc856da199b1a0ce27e4fc0689dfe9f95b9a44217e20176b3a564b753b48', 7, 5, 'free4kids', '[]', 0, '2020-04-01 03:32:29', '2020-04-01 03:32:29', '2021-04-01 11:32:29'),
('e306bc87f23a10b54903f29355c80d4f8e010068d81ce3df27609773e1f9b874a052934023679502', 7, 5, 'free4kids', '[]', 0, '2020-04-01 06:03:51', '2020-04-01 06:03:51', '2021-04-01 14:03:51'),
('eacdb4dd62e8c59e4c0c76e8cb028a7286eb652a7d88b1238da4b524d803a8d88fd9128b8f4e4322', 7, 5, 'free4kids', '[]', 0, '2020-03-30 02:42:22', '2020-03-30 02:42:22', '2021-03-30 10:42:22'),
('f2017cce60392d6ed880527acbd94df2f259894dcc7da104a35682eb70bb04482925157f8e45e89c', 1, 3, 'free4kids', '[]', 0, '2020-02-14 03:33:22', '2020-02-14 03:33:22', '2021-02-14 11:33:22'),
('fe4933bda54113f2955df83f37aebdf3a8bc24b2e2965449f467c04de3cd25415dc3e1baf8dc3774', 7, 5, 'free4kids', '[]', 0, '2020-04-03 06:02:14', '2020-04-03 06:02:14', '2021-04-03 14:02:14');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'qPTABIkCncDqwdNAS0fwzsFZTbtwshHNO4nuvDJO', 'http://localhost', 1, 0, 0, '2019-11-13 22:15:05', '2019-11-13 22:15:05'),
(2, NULL, 'Laravel Password Grant Client', 'skQydJLTkwQn35s8awKEABgxghpdtl5X33Sq0zo6', 'http://localhost', 0, 1, 0, '2019-11-13 22:15:05', '2019-11-13 22:15:05'),
(3, NULL, 'Laravel Personal Access Client', 'EJzRmvqGjHfusz104UO0GteX84iZVEki6Mpc887H', 'http://localhost', 1, 0, 0, '2020-02-14 03:20:34', '2020-02-14 03:20:34'),
(4, NULL, 'Laravel Password Grant Client', 'CdydOzC0w2UU0q3NqHdEMlPxKBJ4SocPubHbnb7n', 'http://localhost', 0, 1, 0, '2020-02-14 03:20:35', '2020-02-14 03:20:35'),
(5, NULL, 'Laravel Personal Access Client', 'Z0vvYnnxIUjlXQTVzltLeuzYO5cLkmxr85vihMRR', 'http://localhost', 1, 0, 0, '2020-03-27 04:00:17', '2020-03-27 04:00:17'),
(6, NULL, 'Laravel Password Grant Client', '46AG9dGJyUc7wK2TX24wJJcp9iAaV68DgpgtU67T', 'http://localhost', 0, 1, 0, '2020-03-27 04:00:18', '2020-03-27 04:00:18');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-11-13 22:15:05', '2019-11-13 22:15:05'),
(2, 3, '2020-02-14 03:20:35', '2020-02-14 03:20:35'),
(3, 5, '2020-03-27 04:00:17', '2020-03-27 04:00:17');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `readings`
--

CREATE TABLE `readings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meter_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reading1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reading2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reading3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reading4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `receivedTime` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `readings`
--

INSERT INTO `readings` (`id`, `meter_id`, `reading1`, `reading2`, `reading3`, `reading4`, `created_at`, `updated_at`, `deleted_at`, `receivedTime`, `user_id`) VALUES
(65, NULL, NULL, NULL, NULL, NULL, '2020-03-16 05:13:37', '2020-03-16 05:13:37', NULL, NULL, NULL),
(66, NULL, NULL, NULL, NULL, NULL, '2020-03-16 05:13:37', '2020-03-16 05:13:37', NULL, NULL, NULL),
(67, '1', '100', '10000', '1000000', '1', '2020-03-16 05:15:24', '2020-03-16 05:15:24', NULL, '2020-02-16 16:00:00', NULL),
(68, '1', '100', '10000', '1000000', '1', '2020-03-16 05:15:24', '2020-03-16 05:15:24', NULL, '2020-02-16 16:00:00', NULL),
(69, '1', '100', '10000', '1000000', '1', '2020-03-16 05:16:17', '2020-03-16 05:16:17', NULL, '2020-02-16 16:00:00', NULL),
(70, '2', '100', '10000', '1000000', '1', '2020-03-16 05:16:17', '2020-03-16 05:16:17', NULL, '2020-02-16 16:00:00', NULL),
(71, '1', '100', '10000', '1000000', '1', '2020-03-16 05:16:43', '2020-03-16 05:16:43', NULL, '2020-02-16 16:00:00', NULL),
(72, '2', '100', '10000', '1000000', '1', '2020-03-16 05:16:43', '2020-03-16 05:16:43', NULL, '2020-02-16 16:00:00', NULL),
(73, '1', '100', '10000', '1000000', '1', '2020-03-16 05:17:07', '2020-03-16 05:17:07', NULL, '2020-02-16 16:00:00', NULL),
(74, '2', '100', '10000', '1000000', '1', '2020-03-16 05:17:07', '2020-03-16 05:17:07', NULL, '2020-02-16 16:00:00', NULL),
(75, '23', '100', '10000', '1000000', '1', '2020-03-16 05:17:07', '2020-03-16 05:17:07', NULL, '2020-02-16 16:00:00', NULL),
(76, '1', '100', '10000', '1000000', '1', '2020-03-16 05:29:29', '2020-03-16 05:29:29', NULL, '2020-02-16 16:00:00', NULL),
(77, '2', '100', '10000', '1000000', '1', '2020-03-16 05:29:29', '2020-03-16 05:29:29', NULL, '2020-02-16 16:00:00', NULL),
(78, '23', '100', '10000', '1000000', '1', '2020-03-16 05:29:29', '2020-03-16 05:29:29', NULL, '2020-02-16 16:00:00', NULL),
(79, '1', '100', '10000', '1000000', '1', '2020-03-16 07:05:52', '2020-03-16 07:05:52', NULL, '2020-02-16 16:00:00', NULL),
(80, '2', '100', '10000', '1000000', '1', '2020-03-16 07:05:52', '2020-03-16 07:05:52', NULL, '2020-02-16 16:00:00', NULL),
(81, '23', '100', '10000', '1000000', '1', '2020-03-16 07:05:52', '2020-03-16 07:05:52', NULL, '2020-02-16 16:00:00', NULL),
(82, '1', '100', '10000', '1000000', '1', '2020-03-16 07:09:00', '2020-03-16 07:09:00', NULL, '2020-02-16 16:00:00', NULL),
(83, '2', '100', '10000', '1000000', '1', '2020-03-16 07:09:00', '2020-03-16 07:09:00', NULL, '2020-02-16 16:00:00', NULL),
(84, '23', '100', '10000', '1000000', '1', '2020-03-16 07:09:00', '2020-03-16 07:09:00', NULL, '2020-02-16 16:00:00', NULL),
(85, '123123', '213', NULL, NULL, NULL, '2020-03-26 01:23:18', '2020-03-26 01:23:18', NULL, '2020-03-23 06:00:06', NULL),
(86, '016724936', '20.60', '52.20', NULL, NULL, '2020-03-26 01:24:16', '2020-03-26 01:24:16', NULL, '2020-03-26 01:19:51', NULL),
(87, '016724936', '23.60', '44.50', NULL, NULL, '2020-03-26 01:24:16', '2020-03-26 01:24:16', NULL, '2020-03-26 01:19:52', NULL),
(88, '016724936', '21.10', '53.50', NULL, NULL, '2020-03-26 01:24:16', '2020-03-26 01:24:16', NULL, '2020-03-26 01:19:53', NULL),
(89, '016724936', '20.60', '52.20', NULL, NULL, '2020-03-26 02:39:14', '2020-03-26 02:39:14', NULL, '2020-03-26 01:19:51', NULL),
(90, '016724936', '23.60', '44.50', NULL, NULL, '2020-03-26 02:39:14', '2020-03-26 02:39:14', NULL, '2020-03-26 01:19:52', NULL),
(91, '016724936', '21.10', '53.50', NULL, NULL, '2020-03-26 02:39:14', '2020-03-26 02:39:14', NULL, '2020-03-26 01:19:53', NULL),
(92, '016724936', '20.60', '52.20', NULL, NULL, '2020-03-26 04:06:12', '2020-03-26 04:06:12', NULL, '2020-03-26 01:19:51', NULL),
(93, '016724936', '23.60', '44.50', NULL, NULL, '2020-03-26 04:06:12', '2020-03-26 04:06:12', NULL, '2020-03-26 01:19:52', NULL),
(94, '016724936', '21.10', '53.50', NULL, NULL, '2020-03-26 04:06:12', '2020-03-26 04:06:12', NULL, '2020-03-26 01:19:53', NULL),
(95, '016724936', '20.60', '52.20', NULL, NULL, '2020-03-26 04:39:34', '2020-03-26 04:39:34', NULL, '2020-03-26 01:19:51', NULL),
(96, '016724936', '23.60', '44.50', NULL, NULL, '2020-03-26 04:39:34', '2020-03-26 04:39:34', NULL, '2020-03-26 01:19:52', NULL),
(97, '016724936', '21.10', '53.50', NULL, NULL, '2020-03-26 04:39:34', '2020-03-26 04:39:34', NULL, '2020-03-26 01:19:53', NULL),
(98, '016724936', '20.60', '52.20', NULL, NULL, '2020-03-26 06:54:22', '2020-03-26 06:54:22', NULL, '2020-03-26 01:19:51', NULL),
(99, '016724936', '23.60', '44.50', NULL, NULL, '2020-03-26 06:54:22', '2020-03-26 06:54:22', NULL, '2020-03-26 01:19:52', NULL),
(100, '016724936', '21.10', '53.50', NULL, NULL, '2020-03-26 06:54:22', '2020-03-26 06:54:22', NULL, '2020-03-26 01:19:53', NULL),
(101, '016724936', '20.60', '52.20', NULL, NULL, '2020-03-26 07:24:34', '2020-03-26 07:24:34', NULL, '2020-03-26 01:19:51', NULL),
(102, '016724936', '23.60', '44.50', NULL, NULL, '2020-03-26 07:24:34', '2020-03-26 07:24:34', NULL, '2020-03-26 01:19:52', NULL),
(103, '016724936', '21.10', '53.50', NULL, NULL, '2020-03-26 07:24:34', '2020-03-26 07:24:34', NULL, '2020-03-26 01:19:53', NULL),
(104, '016724936', '20.60', '52.20', NULL, NULL, '2020-03-27 03:25:07', '2020-03-27 03:25:07', NULL, '2020-03-26 01:19:51', NULL),
(105, '016724936', '23.60', '44.50', NULL, NULL, '2020-03-27 03:25:07', '2020-03-27 03:25:07', NULL, '2020-03-26 01:19:52', NULL),
(106, '016724936', '21.10', '53.50', NULL, NULL, '2020-03-27 03:25:07', '2020-03-27 03:25:07', NULL, '2020-03-26 01:19:53', NULL),
(107, '016724936', '20.60', '52.20', NULL, NULL, '2020-03-27 03:36:43', '2020-03-27 03:36:43', NULL, '2020-03-26 01:19:51', NULL),
(108, '016724936', '23.60', '44.50', NULL, NULL, '2020-03-27 03:36:43', '2020-03-27 03:36:43', NULL, '2020-03-26 01:19:52', NULL),
(109, '016724936', '21.10', '53.50', NULL, NULL, '2020-03-27 03:36:43', '2020-03-27 03:36:43', NULL, '2020-03-26 01:19:53', NULL),
(110, '016724936', '20.60', '52.20', NULL, NULL, '2020-03-27 03:36:56', '2020-03-27 03:36:56', NULL, '2020-03-26 01:19:51', NULL),
(111, '016724936', '23.60', '44.50', NULL, NULL, '2020-03-27 03:36:56', '2020-03-27 03:36:56', NULL, '2020-03-26 01:19:52', NULL),
(112, '016724936', '21.10', '53.50', NULL, NULL, '2020-03-27 03:36:56', '2020-03-27 03:36:56', NULL, '2020-03-26 01:19:53', NULL),
(113, '016724936', '20.60', '52.20', NULL, NULL, '2020-03-27 03:38:12', '2020-03-27 03:38:12', NULL, '2020-03-26 01:19:51', NULL),
(114, '016724936', '23.60', '44.50', NULL, NULL, '2020-03-27 03:38:12', '2020-03-27 03:38:12', NULL, '2020-03-26 01:19:52', NULL),
(115, '016724936', '21.10', '53.50', NULL, NULL, '2020-03-27 03:38:13', '2020-03-27 03:38:13', NULL, '2020-03-26 01:19:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user_name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'jonel', 'jonel', 'jonel@gmail.com', NULL, '$2y$10$tGERgnM7tvbWh.pn7k2NMepRXPF8HaOfvc30WwfP8r9cGxbbFsHb6', NULL, '2020-02-14 03:29:31', '2020-02-14 03:29:31', NULL),
(2, 'product3', 'product3', 'b@gmail.com', NULL, '$2y$10$NeOZCoWXi75FXUzbBjcPL.AuAbnu.R/fWmoD/VH6quJ.CayW5mW1C', NULL, '2020-02-17 00:15:58', '2020-02-17 00:15:58', NULL),
(3, 'asd', 'asd', 'aa@gmail.com', NULL, '$2y$10$zytWEfeYlmDYOvfKSwYUz.irANBWaqLVdAtz5LwoIKaU3Y.ZxU51G', NULL, '2020-03-27 03:55:32', '2020-03-27 03:55:32', NULL),
(4, 'aasd', 'aasd', 'aaa@gmail.com', NULL, '$2y$10$tgF.w6EQlk6z9ukBUaX98e.X930qkCezfg7hp5nGNlXGMWOyTcep6', NULL, '2020-03-27 04:35:30', '2020-03-27 04:35:30', NULL),
(7, 'product3', 'a', 'a@gmail.com', NULL, '$2y$10$XceseQGsdJ5uueSrqwDc9e6doWVS94azP6RLY2s1zmbWhgNXmz.lO', NULL, NULL, NULL, NULL),
(9, 'a', NULL, 'z@gmail.com', NULL, '$2y$10$NBvuOb2kX.YN8/waakQzKOg0n.gCdm8TwAZ1JMMQN1jcj5ikeYowG', NULL, '2020-04-01 03:41:16', '2020-04-01 03:41:16', NULL),
(12, 'a', NULL, 'c@gmail.com', NULL, '$2y$10$unm7Mk3gMFqpDCMmUCqMcOTFhyJNOIzdB2BrGllyZtIA33oAvtoAC', NULL, '2020-04-01 03:52:53', '2020-04-01 03:52:53', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `readings`
--
ALTER TABLE `readings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `readings`
--
ALTER TABLE `readings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
